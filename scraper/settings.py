# Scrapy settings for scraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
import datetime
import os

BOT_NAME = 'Crowdsourcing Scraper'
USER_AGENT = 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)'
ROBOTSTXT_OBEY = False
#COOKIES_ENABLED = False
LOG_LEVEL = 'INFO'
DOWNLOAD_DELAY = 0.25
RANDOMIZE_DOWNLOAD_DELAY = True


SPIDER_MODULES = ['scraper.spiders']
NEWSPIDER_MODULE = 'scraper.spiders'

SPIDER_LOGIN_EMAIL = '<email goes here>'
SPIDER_LOGIN_PW = '<pw goes here>'
SPIDER_LOGIN_PW2 = '<pw goes here>'

# Pipeline
ITEM_PIPELINES = ['scraper.pipelines.InstantExportPipeline']

# IO
TS_FMT = "%Y-%m-%d %H:%M:%S"
PROJECT_PATH = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
IO_PATH = os.path.join(PROJECT_PATH, 'io')

CRAWL_TS = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
