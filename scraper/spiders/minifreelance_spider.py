import datetime
from scrapy import log
from scrapy.http import FormRequest, Request
from scrapy.selector import HtmlXPathSelector
from urlparse import urljoin
from scraper.items import MFJob
from scrapy.spider import BaseSpider
from scraper.utils import number_re, some_txt_re, only_elem, only_elem_or_default, SingleValItemLoader


__author__ = 'amir'


class MFSpider(BaseSpider):
    name = 'minifreelance'
    start_urls = ['http://minifreelance.com/login.php']

    def parse(self, response):
        if 'login_attempted' in response.meta:
            # check login succeed before going on
            if "Email and password don't match" in response.body:
                self.log("Login failed", level=log.ERROR)
                return
            else:
                return Request('http://minifreelance.com/jobs.php?Sort=NEWEST', callback=self.parse_job_list)
        else:
            self.log("Logging in", level=log.INFO)
            settings = self.crawler.settings
            return FormRequest.from_response(response, formdata={'Email': settings['SPIDER_LOGIN_EMAIL'],
                                                                 'Password': settings['SPIDER_LOGIN_PW']},
                                             meta={'login_attempted': True})

    def parse_job_list(self, response):
        """
        Parses Minifreelance Job List and crawls each job page
        """

        self.log("Scraping jobs", level=log.INFO)

        hxs = HtmlXPathSelector(response)
        joblist = hxs.select('//body//div[@id="jobslist"]//tr[@name="RUNNING"]')
        for job in joblist:
            # success percentage is only available on the job list page, so it has to be passed to the task parser as an
            # additional piece of information
            job_success_pct = only_elem_or_default(job.select('td[12]/text()').re(number_re))
            job_url = only_elem(job.select('td[2]//a/@href').extract())
            job_url = urljoin(response.url, job_url)
            yield Request(job_url, callback=self.parse_single_job, meta={'job_success_pct': job_success_pct})

    def parse_single_job(self, response):
        """
        Parses a single Job and produces MFJob item
        """

        self.log('Parsing job %s' % response.url, level=log.DEBUG)
        settings = self.crawler.settings

        hxs = HtmlXPathSelector(response)
        # Abort not running jobs
        if hxs.select('//body//td/text()').re('This job is currently not running'):
            self.log('Aborting not-running job %s' % response.url, level=log.INFO)
            return

        # Fill in the data of the job
        job = SingleValItemLoader(item=MFJob(), response=response)
        job.add_value('timestamp', datetime.datetime.now().strftime(settings['TS_FMT']))
        job.add_value('success_pct', response.meta['job_success_pct'])
        job.add_value('url', response.url)

        parent_node = hxs.select('//body//table[@id="AutoNumber28"]')
        assert len(parent_node) == 1
        job.add_value('name', parent_node.select('(.//span)[1]/text()').re(some_txt_re))
        job.add_value('id', parent_node.select('(.//p)[1]/text()').re(r'Job ID:\s+(\w+)'))
        t = parent_node.select('(.//p)[1]//b/text()').re(number_re)
        job.add_value('work_done', t[0])
        job.add_value('work_total', t[1])
        job.add_value('payment', t[2])
        job.add_value('duration', t[3])
        countries = only_elem(parent_node.select('.//p[contains(b/text(), "You can accept this job if you are from these countries")]/following-sibling::table[1]//td/text()').re(some_txt_re))
        job.add_value('countries', [c.strip() for c in countries.split(',')])
        job.add_value('expected', parent_node.select('.//table[contains(.//b/text(), "What is expected from workers")]/following-sibling::p[following-sibling::table]/text()').extract())
        job.add_value('proof', parent_node.select('.//table[contains(.//td/b/text(), "proof")]/following-sibling::p[following-sibling::p[@style]]/text()').extract())
        yield job.load_item()
