import datetime
from scrapy import log
from scrapy.http import FormRequest, Request
from scrapy.selector import HtmlXPathSelector
from scraper.items import JBJob
from scrapy.spider import BaseSpider
from scraper.utils import number_re, some_txt_re, SingleValItemLoader, only_elem_or_default, only_elem

# from scrapy.shell import inspect_response
# inspect_response(response)

__author__ = 'amir'


class RWSpider(BaseSpider):
    name = 'jobboy'
    start_urls = ['http://www.jobboy.com']

    def parse(self, response):
        if 'login_attempted' in response.meta:
            # check login succeed before going on
            hxs = HtmlXPathSelector(response)
            if hxs.select('//div[@id="error"]//cufontext[contains(text(), "incorrect.")]'):
                self.log("Login failed", level=log.ERROR)
                return
            else:
                self.log("Login succeeded", level=log.DEBUG)
                return Request('http://www.jobboy.com/index.php?inc=user-0&action=jobsavailable', meta={'page': 0},
                               callback=self.parse_job_list)
        else:
            self.log("Logging in", level=log.INFO)
            settings = self.crawler.settings
            return FormRequest.from_response(response,
                                             formdata={'email': settings['SPIDER_LOGIN_EMAIL'],
                                                       'password': settings['SPIDER_LOGIN_PW']},
                                             meta={'login_attempted': True})

    def parse_job_list(self, response):
        """
        Parses Jobboy Job List and crawls each job page
        """

        self.log("Scraping jobs", level=log.INFO)

        hxs = HtmlXPathSelector(response)
        joblist = hxs.select('//body//div[@id="si-content"]//tbody//tr')
        for job in joblist:
            job_success_pct = only_elem_or_default(job.select('./td[4]/text()').re(number_re))
            job_url = only_elem(job.select('.//a/@href').extract())
            yield Request(job_url, callback=self.parse_single_job, meta={'job_success_pct': job_success_pct})
        n_pages = int(only_elem(hxs.select('//body//div[@id="si-content"]//tfoot//tr//text()').re(r'\d+ of\s+(\d+)'))) - 1
        current_p = response.meta['page']
        if current_p < n_pages:
            self.log("Parsing another page of jobs", level=log.DEBUG)
            yield Request('http://www.jobboy.com/index.php?inc=user-%d&action=jobsavailable' % (current_p + 1),
                          meta={'page': current_p + 1}, callback=self.parse_job_list)

    def parse_single_job(self, response):
        """
        Parses a single Job and produces JBJob item
        """

        self.log('Parsing job %s' % response.url, level=log.DEBUG)

        hxs = HtmlXPathSelector(response)
        settings = self.crawler.settings
        # Abort not running jobs
        if hxs.select('//body//td/text()').re('This job is currently not running'):
            self.log('Aborting not-running job %s' % response.url, level=log.INFO)
            return

        job = SingleValItemLoader(item=JBJob(), response=response)
        job.add_value('timestamp', datetime.datetime.now().strftime(settings['TS_FMT']))
        job.add_value('url', response.url)
        job.add_value('success_pct', response.meta['job_success_pct'])

        # Fill in the data of the job
        detail_pane_node = hxs.select('//div[@id="welcome"]//div[@class="left"]')
        t = detail_pane_node.select('.//p/text()').re(number_re)
        job.add_value('work_done', t[0])
        job.add_value('work_total', t[1])
        job.add_value('payment', t[2])
        job.add_value('duration', t[3])
        job.add_value('id', detail_pane_node.select('.//p/text()').re(r'Job ID:\s+(\S+)'))
        job.add_value('name', hxs.select('//div[@id="si-content"]//h1//text()').extract())
        cat_node = hxs.select('(//div[@id="si-content"]/div[@class="signup"]/strong)[1]')
        job.add_value('cat', cat_node.select('text()').re(some_txt_re))
        job.add_value('sub_cat', cat_node.select('(./following-sibling::text())[1]').re(r'(?:-\s+)?(' + some_txt_re + ')'))
        countries = only_elem(hxs.select('//div[@id="si-content"]//strong[contains(text(), "This job is available to")]/following-sibling::text()').re(some_txt_re))
        job.add_value('countries', [c.strip() for c in countries.split('+')])
        job.add_value('expected', hxs.select('//div[@id="si-content"]//strong[contains(text(), "Job Description")]/following-sibling::text()[following-sibling::strong[2]]').extract())
        job.add_value('proof', hxs.select('//div[@id="si-content"]//strong[contains(text(), "How to prove you done it")]/following-sibling::text()[following-sibling::strong[1]]').extract())
        yield job.load_item()
