import datetime
from scrapy import log
from scrapy.http import FormRequest, Request
from scrapy.selector import HtmlXPathSelector
from urlparse import urljoin
from scraper.items import RWJob
from scrapy.spider import BaseSpider
from scraper.utils import number_re, only_elem, SingleValItemLoader

# from scrapy.shell import inspect_response
# inspect_response(response)

__author__ = 'amir'


class RWSpider(BaseSpider):
    name = 'rapidworkers'
    start_urls = ['http://rapidworkers.com/']

    def parse(self, response):
        if 'login_attempted' in response.meta:
            # check login succeed before going on
            if "The login credentials you supplied could not be recognized" in response.body:
                self.log("Login failed", level=log.ERROR)
                return
            else:
                return Request('http://rapidworkers.com/AvailableJobs/sort:modified/direction:desc', callback=self.parse_job_list)
        else:
            self.log("Logging in", level=log.INFO)
            settings = self.crawler.settings
            return FormRequest.from_response(response, formname='loginfrm',
                                             formdata={'data[Member][email]': settings['SPIDER_LOGIN_EMAIL'],
                                                       'data[Member][password]': settings['SPIDER_LOGIN_PW']},
                                             meta={'login_attempted': True})

    def parse_job_list(self, response):
        """
        Parses Rapidworkers Job List and crawls each job page
        """

        self.log("Scraping jobs", level=log.INFO)

        hxs = HtmlXPathSelector(response)
        joblist = hxs.select('//body//div[@class="campaigns index"]//tr[@class="odd" or @class="even"]')
        for job in joblist:
            job_url = only_elem(job.select('td[1]//a/@href').extract())
            job_url = urljoin(response.url, job_url)
            yield Request(job_url, callback=self.parse_single_job)

    def parse_single_job(self, response):
        """
        Parses a single Job and produces RWJob item
        """

        self.log('Parsing job %s' % response.url, level=log.DEBUG)
        settings = self.crawler.settings

        hxs = HtmlXPathSelector(response)
        # Abort not running jobs
        if hxs.select('//body//td/text()').re('This job is currently not running'):
            self.log('Aborting not-running job %s' % response.url, level=log.INFO)
            return

        job = SingleValItemLoader(item=RWJob(), response=response)
        job.add_value('timestamp', datetime.datetime.now().strftime(settings['TS_FMT']))
        job.add_value('url', response.url)

        # Fill in the data of the job
        parent_node = hxs.select('//body//div[@id="jobdetails"]')
        assert len(parent_node) == 1
        job.add_value('id', parent_node.select('(div[1]/ul)[1]/li/text()').re(r'Campaign ID\s*:\s+(\S+)'))
        t = parent_node.select('(div[1]/ul)[1]/li//text()').re(number_re)
        job.add_value('work_done', t[0])
        job.add_value('work_total', t[1])
        job.add_value('payment', t[2])
        job.add_value('duration', t[3])
        job.add_value('name', parent_node.select('(div/ul)[1]/li/text()').re(r'Campaign Name\s*:\s+(.+)'))
        job.add_value('countries', parent_node.select('(div[2]/ul)[1]/li/text()').re('\w+'))
        job.add_value('expected', parent_node.select('div/h3[contains(text(), "What is expected from workers")]/following-sibling::p//text()').extract())
        job.add_value('proof', parent_node.select('div/h3[contains(text(), "Required proof that task was finished")]/following-sibling::p//text()').extract())
        yield job.load_item()
