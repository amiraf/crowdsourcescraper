import datetime
from scrapy import log
from scrapy.http import FormRequest, Request
from scrapy.selector import HtmlXPathSelector
from urlparse import urljoin
from scraper.items import MWJob, MWUser
import re
from scrapy.spider import BaseSpider
from scraper.utils import country_re, number_re, some_txt_re, SingleValItemLoader, only_elem


__author__ = 'amir'
xtract_id = re.compile(r'(?<=Id=)\w+')


class MWSpider(BaseSpider):
    name = 'microworkers'
    start_urls = ['https://microworkers.com/login.php']

    def parse(self, response):
        if 'login_attempted' in response.meta:
            # check login succeed before going on
            if "Email and password don't match" in response.body:
                self.log("Login failed", level=log.ERROR)
                return
            else:
                return Request('https://microworkers.com/jobs.php?Sort=NEWEST&Id_category=ALL', callback=self.parse_job_list)
        else:
            self.log("Logging in", level=log.INFO)
            settings = self.crawler.settings
            return FormRequest.from_response(response, formdata={'Email': settings['SPIDER_LOGIN_EMAIL'],
                                                                 'Password': settings['SPIDER_LOGIN_PW']},
                                             meta={'login_attempted': True})

    def parse_job_list(self, response):
        """
        Parses Microworkers Job List and crawls each job page
        """

        self.log("Scraping jobs and users", level=log.INFO)

        hxs = HtmlXPathSelector(response)
        joblist = hxs.select('//body//div[@id="jobslist"]//tr[@name="RUNNING"]')
        for job in joblist:
            # success percentage is only available on the job list page, so it has to be passed to the task parser as an
            # additional piece of information
            job_success_pct = only_elem(job.select('td[4]/text()').extract()).strip()
            job_url = only_elem(job.select('td[2]//a/@href').extract())
            job_url = urljoin(response.url, job_url)
            yield Request(job_url, callback=self.parse_single_job, meta={'job_success_pct': job_success_pct})

    def parse_single_job(self, response):
        """
        Parses a single Job and produces MWJob item
        """

        self.log('Parsing job %s' % response.url, level=log.DEBUG)
        settings = self.crawler.settings

        hxs = HtmlXPathSelector(response)
        # Abort not running jobs
        if hxs.select('//body//td/text()').re('This job is currently not running'):
            self.log('Aborting not-running job %s' % response.url, level=log.INFO)
            return

        # Fill in the data of the job
        job = SingleValItemLoader(item=MWJob(), response=response)
        job.add_value('timestamp', datetime.datetime.now().strftime(settings['TS_FMT']))
        job.add_value('success_pct', response.meta['job_success_pct'])
        job.add_value('url', response.url)

        parent_node = hxs.select('//body//table//table//table//td[@width="520"]')
        assert len(parent_node) == 1
        job.add_value('name', parent_node.select('span[1]/text()').extract())
        job.add_value('id', parent_node.select('table[2]//td[1]/p/text()').re(r'Job ID:\s+(\w+)'))
        t = parent_node.select('table[2]//td[1]/p//b/text()').re(number_re)
        job.add_value('work_done', t[0])
        job.add_value('work_total', t[1])
        job.add_value('payment', t[2])
        job.add_value('duration', t[3])
        href = only_elem(parent_node.select('table[2]//a/@href').extract())
        job.add_value('employer', xtract_id.search(href).group(0))
        job.add_value('countries', parent_node.select('table[3]//p/text()').re(country_re))
        job.add_value('cat', parent_node.select('p[1]//b/text()').re(some_txt_re))
        job.add_value('sub_cat', parent_node.select('p[1]/text()').re(some_txt_re))
        job.add_value('expected', parent_node.select('table[4]//td[1]/p[1]//text()').extract())
        job.add_value('proof', parent_node.select('table[4]//td[1]/p[3]//text()').extract())
        yield job.load_item()
        user_url = urljoin(response.url, href)
        yield Request(user_url, callback=self.parse_single_user)

    def parse_single_user(self, response):
        """
        Parses a single page of user
        """

        self.log('Parsing user %s' % response.url, level=log.DEBUG)
        settings = self.crawler.settings

        user = SingleValItemLoader(item=MWUser(), response=response)
        user.add_value('timestamp', datetime.datetime.now().strftime(settings['TS_FMT']))
        user.add_value('url', response.url)

        hxs = HtmlXPathSelector(response)

        # fill in the details of user
        user.add_value('name', hxs.select('//table[@id="AutoNumber64"]//td[contains(text(), "Nickname")]/following-sibling::td/b/text()').extract())
        user.add_value('id', hxs.select('//table[@id="AutoNumber64"]//td[contains(text(), "ID")]/following-sibling::td/text()').extract())
        user.add_value('member_since', hxs.select('//table[@id="AutoNumber64"]//td[contains(text(), "Member since")]/following-sibling::td/text()').extract())
        user.add_value('country', hxs.select('//table[@id="AutoNumber64"]//td[contains(text(), "Country")]/following-sibling::td/text()').re(some_txt_re))
        user.add_value('city', hxs.select('//table[@id="AutoNumber64"]//td[contains(text(), "City")]/following-sibling::td/text()').re(some_txt_re))
        user.add_value('total_earned', hxs.select('//table[@id="AutoNumber91"]//td[contains(b/text(), "Total Earned")]/following-sibling::td/b/text()').re(number_re))
        user.add_value('tasks_done', hxs.select('//table[@id="AutoNumber91"]//td[contains(text(), "Tasks done")]/following-sibling::td/text()').re(number_re))
        user.add_value('basic_tasks', hxs.select('//table[@id="AutoNumber92"]//td[contains(b/text(), "Basic tasks")]/following-sibling::td/b/text()').re(number_re))
        user.add_value('basic_satisfied', hxs.select('//table[@id="AutoNumber92"]//td[contains(text(), "Satisfied") and not(contains(text(), "Not"))]/following-sibling::td/text()').re(number_re))
        user.add_value('basic_not_satisfied', hxs.select('//table[@id="AutoNumber92"]//td[contains(text(), "Not-Satisfied")]/following-sibling::td/text()').re(number_re))
        user.add_value('basic_avg_per_task', hxs.select('//table[@id="AutoNumber92"]//td[contains(text(), "Average per task")]/following-sibling::td/text()').re(number_re))
        user.add_value('hg_tasks', hxs.select('//table[@id="AutoNumber93"]//td[contains(b/text(), "Hire Group")]/following-sibling::td/b/text()').re(number_re))
        user.add_value('hg_satisfied', hxs.select('//table[@id="AutoNumber93"]//td[contains(text(), "Satisfied") and not(contains(text(), "Not"))]/following-sibling::td/text()').re(number_re))
        user.add_value('hg_not_satisfied', hxs.select('//table[@id="AutoNumber93"]//td[contains(text(), "Not-Satisfied")]/following-sibling::td/text()').re(number_re))
        user.add_value('hg_avg_per_task', hxs.select('//table[@id="AutoNumber93"]//td[contains(text(), "Average per task")]/following-sibling::td/text()').re(number_re))
        user.add_value('basic_campaigns', hxs.select('//table[@id="AutoNumber95"]//td[contains(b/text(), "Basic Campaigns")]/following-sibling::td/b/text()').re(number_re))
        user.add_value('basic_tasks_paid', hxs.select('(//table[@id="AutoNumber95"]//td[contains(text(), "Tasks paid")])[1]/following-sibling::td/text()').re(number_re))
        user.add_value('basic_total_paid', hxs.select('(//table[@id="AutoNumber95"]//td[contains(text(), "Total paid")])[1]/following-sibling::td/text()').extract())
        user.add_value('hg_campaigns', hxs.select('//table[@id="AutoNumber95"]//td[contains(b/text(), "Hire Group")]/following-sibling::td/b/text()').re(number_re))
        user.add_value('hg_tasks_paid', hxs.select('(//table[@id="AutoNumber95"]//td[contains(text(), "Tasks paid")])[2]/following-sibling::td/text()').re(number_re))
        user.add_value('hg_total_paid', hxs.select('(//table[@id="AutoNumber95"]//td[contains(text(), "Total paid")])[2]/following-sibling::td/text()').extract())
        user.add_value('employer_type', hxs.select('//table[@id="AutoNumber95"]//td[contains(b/text(), "Employer type")]/following-sibling::td/text()').re(some_txt_re))
        yield user.load_item()
